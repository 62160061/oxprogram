/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tictactoe;

import java.util.Scanner;

/**
 *
 * @author ModG
 */
public class OXProgram {

    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            // System.out.println("row :"+row + "col: "+ col);
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
        showTable();

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkrighttoleft();
        checklefttoright();
    }

    static void checkrighttoleft() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checklefttoright() {
        int R = 2;
        for (int row = 0; row < 3; row++, R--) {
            if (table[row][R] != player) {
                return;
            }

        }
        isFinish = true;
        winner = player;
    }

    static void checkDraw() {

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }

    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw");
        } else {
            System.out.println(winner + " Win!!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye . . . .");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

}
